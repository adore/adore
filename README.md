### Hello!
I am adore! I am a software engineer and have a few years of experience. 

![](https://komarev.com/ghpvc/?username=mesh&style=plastic&color=blueviolet&label=Viewers)

Some skills I know include:

Languages:

![](https://img.shields.io/badge/HTML5-E34F26?style=plastic&logo=html5&logoColor=white)
![](https://img.shields.io/badge/CSS3-1572B6?style=plastic&logo=css3&logoColor=white)
![](https://img.shields.io/badge/JavaScript-323330?style=plastic&logo=javascript&logoColor=F7DF1E)
![](https://img.shields.io/badge/TypeScript-007ACC?style=plastic&logo=typescript&logoColor=white)
![](https://img.shields.io/badge/C-00599C?style=plastic&logo=c&logoColor=white)
![](https://img.shields.io/badge/C%2B%2B-00599C?style=plastic&logo=c%2B%2B&logoColor=white)
![](https://img.shields.io/badge/Java-ED8B00?style=plastic&logo=java&logoColor=white)
![](https://img.shields.io/badge/PHP-777BB4?style=plastic&logo=php&logoColor=white)

Databases:

![](https://img.shields.io/badge/MySQL-005C84?style=plastic&logo=mysql&logoColor=white)
![](https://img.shields.io/badge/PostgreSQL-316192?style=plastic&logo=postgresql&logoColor=white)
![](https://img.shields.io/badge/MongoDB-4EA94B?style=plastic&logo=mongodb&logoColor=white)
![](https://img.shields.io/badge/SQLite-07405E?style=plastic&logo=sqlite&logoColor=white)
![](https://img.shields.io/badge/Redis-%23DD0031.svg?&style=plastic&logo=redis&logoColor=white)

ORMs:

![](https://img.shields.io/badge/Prisma-3982CE?style=plastic&logo=Prisma&logoColor=white)

Frameworks:

![](https://img.shields.io/badge/React_Native-20232A?style=plastic&logo=react&logoColor=61DAFB)
![](https://img.shields.io/badge/Node.js-339933?style=plastic&logo=nodedotjs&logoColor=white)
![](https://img.shields.io/badge/Express.js-000000?style=plastic&logo=express&logoColor=white)
![](https://img.shields.io/badge/Next.js-000000?style=plastic&logo=nextdotjs&logoColor=white)
![](https://img.shields.io/badge/Nuxt.js-00C58E?style=plastic&logo=nuxtdotjs&logoColor=white)
![](https://img.shields.io/badge/Vue.js-35495E?style=plastic&logo=vuedotjs&logoColor=4FC08D)
![](https://img.shields.io/badge/Sass-CC6699?style=plastic&logo=sass&logoColor=white)
![](https://img.shields.io/badge/React-20232A?style=plastic&logo=react&logoColor=61DAFB)
![](https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=plastic&logo=tailwind-css&logoColor=white)
![](https://img.shields.io/badge/Bootstrap-563D7C?style=plastic&logo=bootstrap&logoColor=white)

IDEs:

![](https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=plastic&logo=visual%20studio%20code&logoColor=white)
![](https://img.shields.io/badge/Visual_Studio-5C2D91?style=plastic&logo=visual%20studio&logoColor=white)

Contact me:

![](https://discord.c99.nl/widget/theme-4/582284378802225152.png)

![](https://img.shields.io/badge/xoadores-blueviolet?style=plastic&logo=instagram&logoColor=white)
![](https://img.shields.io/badge/xoadores-blueviolet?style=plastic&logo=twitter&logoColor=white)
![](https://img.shields.io/badge/mesh-blueviolet?style=plastic&logo=github&logoColor=white)
